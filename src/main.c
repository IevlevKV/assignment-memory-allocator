#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void* heap;

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}


void test1(){
	printf("--------------------\n");
	printf("Выделение одного блока\n");
	void* block1 = _malloc(2222);
	if (block1 == NULL){
		printf("Память не была выделенна!\n");
	}
	if (block_get_header(block1) -> capacity.bytes != 2222){
		printf("Выделился блок неправильного размера!\n");
	}
	_free(block1);
	printf("Тест пройден!\n");
	
	
	}


void test2(){
	printf("--------------------\n");

	void* block1 = _malloc(2000);
	void* block2 = _malloc(1660);
	void* block3 = _malloc(5204);
		
	printf("Освобождение одного блока из нескольких выделенных.\n");

	if (block1 == NULL || block2 == NULL || block3 == NULL){
		printf("Память не выделена!\n");
	}
		

	_free(block2);
	struct block_header *header1 = block_get_header(block1);
	struct block_header *header2 = block_get_header(block2);
	struct block_header *header3 = block_get_header(block3);

	if (header1 -> is_free == false || header2 -> is_free == true || header3 -> is_free == false){
	printf("Тест пройден!\n");
	}else{
		_free(block1);
		_free(block3);
		printf("Тест не пройден!\n");
	}
		
}

void test3(){
	printf("--------------------\n");

	void* block1 = _malloc(1858);
	void* block2 = _malloc(1427);
	void* block3 = _malloc(2252);
		
	printf("Освобождение двух блоков из нескольких выделенных.\n");

	if (block1 == NULL || block2 == NULL || block3 == NULL){
		printf("Память не выделена!\n");
	}
		

	_free(block2);
	_free(block1);
	struct block_header *header1 = block_get_header(block1);
	struct block_header *header2 = block_get_header(block2);
	struct block_header *header3 = block_get_header(block3);

	if (header1 -> is_free == true || header2 -> is_free == true || header3 -> is_free == false){
	printf("Тест пройден!\n");
	}else{
		_free(block3);
		printf("Тест не пройден!\n");
	}
}


void test4(){
	printf("--------------------\n");
	printf("Память закончилась, новый регион памяти расширяет старый.\n");
	void* block1 = _malloc(10000);
	void* block2 = _malloc(10000);

	if (block1 == NULL || block2 == NULL){
		printf("Память не выделена!\n");
	}

	struct block_header *header1 = block_get_header(block1);
	struct block_header *header2 = block_get_header(block2);


	if (blocks_continuous(header1, header2)){
		printf("Тест пройден!\n");

	}else{printf("Тест не пройден!\n");}
	
	_free(block2);
	_free(block1);

		


}

void test5(struct block_header *first_block){
	printf("--------------------\n");
	printf("Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");
	
	while (first_block->next != NULL){
		 first_block = first_block->next;}
   	map_pages(first_block, 1000, MAP_FIXED);

	void* block1 = _malloc(10000);
		
	if (block1 == NULL){
		printf("Память не была выделенна!\n");
	}

	struct block_header* header1 = block_get_header(block1);
	
	if(first_block == header1){
		printf("Тест не пройден!\n");
	}else{printf("Тест пройден!\n");}
	
	_free(block1);
}



int main(){
	heap = heap_init(10000);
	struct block_header *first_block = (struct block_header *) heap;
	test1();
	test2();
	test3();
	test4();
	test5(first_block);
	return 0;
}
